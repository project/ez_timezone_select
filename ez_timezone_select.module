<?php

/** Core hooks ***************************************************************/

/**
 * Implements hook_element_info_alter().
 */
function ez_timezone_select_element_info_alter(&$info) {
  // Select fields.
  $info['select']['#process'][] = 'ez_timezone_select_element_process';

  // Date API timezone fields.
  if (module_exists('date_api')) {
    $info['date_timezone']['#process'][] = 'ez_timezone_select_element_process';
    $info['date_timezone']['#ez_timezone_select'] = TRUE;
  }
}

/** Form alters **************************************************************/

/**
 * Implements hook_form_FORM_ID_alter() for "system_regional_settings".
 */
function ez_timezone_select_form_system_regional_settings_alter(&$form, &$form_state) {
  $form['timezone']['date_default_timezone']['#ez_timezone_select'] = TRUE;
}

/**
 * Implements hook_form_FORM_ID_alter() for "user_register_form".
 */
function ez_timezone_select_form_user_register_form_alter(&$form, &$form_state) {
  // System module adds timezone field in form alter, so add process callback.
  $form['#process'][] = 'ez_timezone_select_user_form_process';
}

/**
 * Implements hook_form_FORM_ID_alter() for "user_profile_form".
 */
function ez_timezone_select_form_user_profile_form_alter(&$form, &$form_state) {
  // System module adds timezone field in form alter, so add process callback.
  $form['#process'][] = 'ez_timezone_select_user_form_process';
}

/** Process callbacks ********************************************************/

/**
 * Process callback for "user_register_form" and "user_profile_form".
 */
function ez_timezone_select_user_form_process($element, &$form_state) {
  // Timezone may be hidden based on regional settings.
  if (!empty($element['timezone']['timezone'])) {
    $element['timezone']['timezone']['#ez_timezone_select'] = TRUE;
  }
  return $element;
}

/**
 * Process callback that places common options at top of timezone selects.
 */
function ez_timezone_select_element_process($element) {
  // Skip.
  if (empty($element['#ez_timezone_select'])) {
    return $element;
  }

  // Get parent element containing options.
  switch ($element['#type']) {
    // Select.
    case 'select':
      $parent = &$element;
      break;

    // Date API timezone.
    case 'date_timezone':
      $parent = &$element['timezone'];
      break;

    // Invalid type.
    default:
      return $element;
  }

  // Build empty option (if available).
  $empty = isset($parent['#options']['']) ? array('' => $parent['#options']['']) : array();

  // US options.
  $us = array(
    'America/New_York' => t('Eastern Time') . ' (America/New_York)',
    'America/Chicago' => t('Central Time') . ' (America/Chicago)',
    'America/Denver' => t('Mountain Time') . ' (America/Denver)',
    'America/Phoenix' => t('Mountain Time - Arizona') . ' (America/Phoenix)',
    'America/Los_Angeles' => t('Pacific Time') . ' (America/Los_Angeles)',
    'America/Anchorage' => t('Alaska Time') . ' (America/Anchorage)',
    'Pacific/Honolulu' => t('Hawaii Time') . ' (Pacific/Honolulu)',
  );

  // Strip US options from World options.
  $world = array_diff_key($parent['#options'], array('' => ''), $us);

  // Place US options above World options.
  $parent['#options'] = $empty + array(
    t('US') => $us,
    t('World') => $world,
  );

  return $element;
}
